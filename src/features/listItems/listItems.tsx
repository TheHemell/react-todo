import React, { useState } from 'react';
import { FormControlLabel, Checkbox, Button, TextField, Paper, Typography } from "@mui/material";

export interface ListType {
  id: number;
  label: string;
  status: boolean;
}

export function List () {
  const [getItems, setState] = useState([
    {
      id: 1,
      label: 'Go work',
      status: false
    },
    {
      id: 2,
      label: 'Working',
      status: false
    },
    {
      id: 3,
      label: 'Go Home',
      status: false
    },
  ]);

  const [getValue, setValue] = useState('');

  function changeStatus(item: ListType) {
    let currentTask = getItems.map(task => {
      if (task.id === item.id) {
         task.status = !task.status
      }

      return task
    })

    if (!currentTask) {
      return false
    }

    setState(currentTask)
  }

  function deleteItem(item: ListType) {
    getItems.splice(getItems.indexOf(item), 1)

    let newArray = [ ...getItems ]

    setState(newArray)
  }

  function addItem() {
    getItems.push({
      id: getItems.length + 1,
      label: getValue,
      status: true,
    })

    let newArray = [ ...getItems ]

    setState(newArray)
  }

  let elItems = getItems.map((item:ListType) => (
      <div key={ item.id }>
        <FormControlLabel control={<Checkbox checked={item.status} />} label={ item.label } onClick={() => changeStatus(item)} />
        <Button variant="contained" color="error" onClick={() => deleteItem(item)}>Delete</Button>
      </div>
    )
  );

  return (
    <div>
      <Paper sx={{ p: 3, mt: 3, mb: 3 }}>
        <Typography variant="h5" sx={{ mb: 2 }}>
          My List
        </Typography>
        <div className="ListItems">
          { elItems }
        </div>
        <div>Кол-во задач: { getItems.length }</div>
      </Paper>
      <Paper sx={{ p: 3, mb: 3 }} className="Form">
        <TextField
          id="outlined-basic"
          label="Task name"
          variant="outlined"
          value={getValue}
          onChange={(e) => setValue(e.target.value)}
          sx={{ mb: 3 }}
        />
        <Button variant="contained" color="primary" onClick={addItem}>Add</Button>
      </Paper>
    </div>
  )
}
