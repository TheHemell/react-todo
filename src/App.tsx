import React from 'react';

import './App.css';

import { List } from './features/listItems/listItems';

function App() {
  return (
    <div className="App">
      <div className="List">
        <List />
      </div>
    </div>
  );
}

export default App;
